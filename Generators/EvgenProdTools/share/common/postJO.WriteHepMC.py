#  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
## Print the output by WriteHepMC
from TruthIO.TruthIOConf import WriteHepMC
topAlg += WriteHepMC()
