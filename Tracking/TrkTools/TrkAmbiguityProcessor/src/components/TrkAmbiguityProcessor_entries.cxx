#include "../SimpleAmbiguityProcessorTool.h"
#include "../TrackSelectionProcessorTool.h"
#include "../DenseEnvironmentsAmbiguityProcessorTool.h"
#include "../DenseEnvironmentsAmbiguityScoreProcessorTool.h"

using namespace Trk;
DECLARE_COMPONENT( DenseEnvironmentsAmbiguityProcessorTool )
DECLARE_COMPONENT( DenseEnvironmentsAmbiguityScoreProcessorTool )
DECLARE_COMPONENT( SimpleAmbiguityProcessorTool )
DECLARE_COMPONENT( TrackSelectionProcessorTool )

