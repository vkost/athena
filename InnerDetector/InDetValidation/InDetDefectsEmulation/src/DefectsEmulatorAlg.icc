/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/
#include "Identifier/Identifier.h"
#include "AtlasDetDescr/AtlasDetectorID.h"
#include "EventContainers/IdentifiableContainerBase.h"
#include "InDetRawData/InDetRawDataCollection.h"
#include "InDetRawData/SCT3_RawData.h"
#include "InDetRawData/Pixel1RawData.h"

#include "StoreGate/WriteHandle.h"

#include <vector>
#include <memory>
#include <mutex>

namespace InDet{

  template <class T_RDO_Container>
  StatusCode DefectsEmulatorAlg<T_RDO_Container>::initialize(){
     ATH_CHECK( m_emulatedDefects.initialize() );
     ATH_CHECK( m_rdoOutContainerKey.initialize() );
     ATH_CHECK( m_origRdoContainerKey.initialize() );
     ATH_CHECK( detStore()->retrieve(m_idHelper, m_idHelperName.value()) );

     return DefectsEmulatorBase::initializeBase(m_idHelper->wafer_hash_max());
  }

  template <class T_RDO_Container>
  StatusCode DefectsEmulatorAlg<T_RDO_Container>::execute(const EventContext& ctx) const {
    SG::ReadCondHandle<T_DefectsData> emulatedDefects(m_emulatedDefects,ctx);
    ATH_CHECK(emulatedDefects.isValid());
    SG::ReadHandle<T_RDO_Container> origRdoContainer(m_origRdoContainerKey, ctx);
    ATH_CHECK(origRdoContainer.isValid());
    SG::WriteHandle<T_RDO_Container> rdoOutContainer(m_rdoOutContainerKey, ctx);
    ATH_CHECK( rdoOutContainer.record (std::make_unique<T_RDO_Container>(origRdoContainer->size(), EventContainers::Mode::OfflineFast)) );

    unsigned int n_rejected=0u;
    unsigned int n_split_rdos=0u;
    unsigned int n_new=0;
    T_ID_Adapter id_helper(m_idHelper);

    std::vector<typename T_ModuleHelper::KEY_TYPE> sorted_keys;
    for(const InDetRawDataCollection<T_RDORawData>* collection : *origRdoContainer ) {
       const IdentifierHash idHash = collection->identifyHash();
       T_ModuleHelper module_helper( emulatedDefects->getDetectorElement(idHash).design() );
       std::unique_ptr<InDetRawDataCollection<T_RDORawData> >
          clone = std::make_unique<InDetRawDataCollection<T_RDORawData> >(idHash);
       clone->setIdentifier(collection->identify());
       unsigned int rejected_per_mod=0u;
       if (!emulatedDefects->isModuleDefect(idHash)) {
          clone->reserve( collection->size());
          if (!module_helper) {
             ATH_MSG_ERROR( "Not module design for " << idHash);
             return StatusCode::FAILURE;
          }
          TH2 *h2_rejected_hits=nullptr;
          if (m_histogrammingEnabled) {
             std::lock_guard<std::mutex> lock(m_histMutex);
             h2_rejected_hits = findHist(module_helper.nSensorRows(), module_helper.nSensorColumns());
          }

          sorted_keys.clear();
          sorted_keys.reserve( collection->size());
          for(const auto *const rdo : *collection) {
             const Identifier rdoID = rdo->identify();

             auto row_idx = id_helper.row_index(rdoID);
             auto col_idx = id_helper.col_index(rdoID);

             unsigned int n_new_hits = id_helper.cloneOrRejectHit( module_helper, *(emulatedDefects.cptr()), idHash, row_idx, col_idx, *rdo, *clone);
             if (n_new_hits>0) {
                n_split_rdos += (n_new_hits-1);
                for (unsigned int hit_i=n_new_hits; hit_i>0; --hit_i) {
                   auto new_hit = clone->at(clone->size()-hit_i);

                   const Identifier rdoID = new_hit->identify();
                   auto row_idx = id_helper.row_index(rdoID);
                   auto col_idx = id_helper.col_index(rdoID);
                   unsigned int key = module_helper.hardwareCoordinates(row_idx,col_idx);
                   auto iter = std::lower_bound(sorted_keys.begin(), sorted_keys.end(), key);
                   if (iter == sorted_keys.end()) {
                      sorted_keys.push_back(key);
                   }
                   else {
                      if (*iter != key) {
                         sorted_keys.insert(iter, key);
                      }
                   }
                }
             }
             else {
                if (m_histogrammingEnabled && h2_rejected_hits) {
                   std::lock_guard<std::mutex> lock(m_histMutex);
                   h2_rejected_hits->Fill(col_idx, row_idx);
                }
                ++rejected_per_mod;
             }

          }
       }
       else {
          rejected_per_mod += collection->size();
       }
       if (m_histogrammingEnabled) {
          std::lock_guard<std::mutex> lock(m_histMutex);
          unsigned int bin_i=m_moduleHist[kRejectedHits]->GetBin( idHash%100+1, idHash/100+1);
          m_moduleHist[kRejectedHits]->SetBinContent(bin_i, rejected_per_mod );
       }
       n_rejected += rejected_per_mod;
       n_new += clone->size();
       rdoOutContainer->addCollection( clone.release(), idHash).ignore();
    }
    m_rejectedRDOs += n_rejected;
    m_totalRDOs += n_new;
    m_splitRDOs += n_split_rdos;
    ATH_MSG_DEBUG("rejected  " << m_rejectedRDOs << ", copied " << m_totalRDOs << " RDOs"
                 << " split hits: " << n_split_rdos);

    return StatusCode::SUCCESS;
  }

}
