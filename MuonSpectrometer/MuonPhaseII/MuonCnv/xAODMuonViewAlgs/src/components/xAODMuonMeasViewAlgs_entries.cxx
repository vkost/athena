
/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "../sTgcMeasViewAlg.h"
#include "../RpcMeasViewAlg.h"
#include "../MdtMeasViewAlg.h"
#include "../SegmentViewAlg.h"
DECLARE_COMPONENT(MuonR4::sTgcMeasViewAlg)
DECLARE_COMPONENT(MuonR4::RpcMeasViewAlg)
DECLARE_COMPONENT(MuonR4::MdtMeasViewAlg)
DECLARE_COMPONENT(MuonR4::SegmentViewAlg)