/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODMUONPREPDATA_MMClusterAUXCONTAINER_H
#define XAODMUONPREPDATA_MMClusterAUXCONTAINER_H

#include "xAODMuonPrepData/MMClusterFwd.h"
#include "xAODMuonPrepData/versions/MMClusterAuxContainer_v1.h"

// Set up a CLID for the class:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF(xAOD::MMClusterAuxContainer, 1268473872, 1)
#endif  // XAODMUONRDO_NMMRDO_H
